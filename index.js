var fs = require('fs');
var nativePath = require('path');
var path = require('path').posix;

var UriTemplateCodegen = require('uri-templates-codegen');

var amdCodeTemplate = fs.readFileSync(__dirname + '/tiny-amd-template.js', {encoding: 'utf-8'});
function amdCode(options) {
    var lookupCode = 'function (moduleName) {return currentScriptBase + encodeURIComponent(moduleName);}';
    if (typeof options.template === 'string') {
        var template = UriTemplateCodegen(options.template, {object: true});
        lookupCode = 'function (moduleName){return ' + template.fillCode + '({name:moduleName})}';
    } else if (options.template && typeof options.template === 'object') {
        var map = UriTemplateCodegen.Map(options.template);
        lookupCode = map.forwardCode;
    }
    var replacements = {
        LOOKUP_FUNCTION: lookupCode
    };
    var jsCode = amdCodeTemplate;
    for (var key in replacements) {
        jsCode = jsCode.split(key).join(replacements[key]);
    }
    return jsCode;
}

function calculateEtag(code) {
    var hash = require('crypto').createHash('sha256');
    hash.update(code);
    return JSON.stringify(hash.digest('base64'));
}

function normaliseOptions(options) {
    var result = {
        amd: {}
    };
    if (typeof options === 'string') {
        result.url = options;
    } else {
        for (var key in options || {}) {
            result[key] = options[key];
        }
    }
    if (typeof result.path === 'string' || Array.isArray(result.path)) {
        result.path = {
            "": result.path
        };
    }
    return result;
}

var defaultExtensions = {
    json: function (source, canonicalName, relativeName, callback) {
        return 'define([],' + source + ');'
    },
    js: function (jsCode, canonicalName, relativeName, callback) {
        // CommonJS
        var dependencies = {};
        jsCode = jsCode.replace(/(require\s*\(\s*)("([^"]|\\\.])*"|'([^']|\\\.])*')(\s*\))/g, function (match, prefix, depExpr, innerQuot2, innerQuot1, suffix) {
            var depName = eval(depExpr); // Sanitised by regex
            var resolved = depName;
            if (depName[0] === '.') {
                resolved = path.join(relativeName, depName);
            }
            dependencies[resolved] = depName;
            return prefix + JSON.stringify(resolved) + suffix;
        });
        var depList = Object.keys(dependencies);

        var factoryCode = 'function(){';
        factoryCode += 'var module={exports:{}},exports=module.exports,global=this;'
        factoryCode += jsCode;
        factoryCode += '\nreturn module.exports;\n}';
        return 'define(' + JSON.stringify(depList) + ',' + factoryCode + ');';
    }
};

function resolve(moduleName) {
    return require.resolve(moduleName);
}

function transformIfNeeded(jsCode, options, callback) {
    if (typeof options.transform !== 'function') {
        return callback(null, jsCode);
    }
    var callbackCalled = false;
    try {
        var transformed = options.transform(jsCode, function (error, transformed) {
            if (callbackCalled) return;
            callback(error, transformed);
        });
    } catch (e) {
        callbackCalled = true;
        callback(e);
    }
    if (typeof transformed === 'string') {
        callbackCalled = true;
        callback(null, transformed);
    }
}

module.exports = function (optionsArg) {
    var options = normaliseOptions(optionsArg);
    var extensions = Object.create(defaultExtensions);
    for (var key in options.extensions || {}) {
        extensions[key] = options.extensions[key];
    }

    var amdServerCode = amdCodeTemplate.replace(/CLIENT START(.|[\r\n])*CLIENT END/, 'client code removed');
    var amdServerFunction = new Function('missingModule', amdServerCode + '\nreturn {define: define, require: require};');
    var amdServer = amdServerFunction(loadAmdModule);
    var pendingModules = {};
    function loadAmdModule(moduleName) {
        if (pendingModules[moduleName]) return;
        pendingModules[moduleName] = true;
        lookupCode(moduleName, function (error, jsCode) {
            if (error) throw error;
            jsCode = 'define._n=' + JSON.stringify(moduleName) + ';' + jsCode;
            var evalFunc = new Function('define', 'require', 'module', 'exports', jsCode);
            evalFunc(amdServer.define, amdServer.require, null, null);
        });
    }

    function lookupCode(moduleName, callback) {
        // Absolute paths are not allowed
        if (nativePath.resolve(moduleName) === nativePath.normalize(moduleName)) {
            return callback(new Error('modules cannot be absolute'));
        }

        var relativeModuleName = moduleName;

        var aliasedModuleNames = [path.normalize(moduleName)];
        if (/^\.[\\/]/.test(moduleName)) {
            aliasedModuleNames[0] = './' + aliasedModuleNames[0];
        }
        var aliases = options.path || {};
        var aliasKeys = Object.keys(aliases).sort().reverse();
        for (var i = 0; i < aliasKeys.length; i++) {
            var key = aliasKeys[i];
            var trimmedKey = key.replace(/\/$/, '');
            if (moduleName === trimmedKey) {
                aliasedModuleNames = [].concat(aliases[key]);
                break;
            } else if (!trimmedKey || moduleName.substring(0, trimmedKey.length + 1) === trimmedKey + '/') {
                aliasedModuleNames = [].concat(aliases[key]).map(function (alias) {
                    var trimmedAlias = alias.replace(/[\/\\]*$/, '');
                    return trimmedAlias + '/' + moduleName.replace(trimmedKey, '').replace(/^\/|\/$/, '');
                });
                break;
            }
        }

        var resolveError = null;
        for (var i = 0; i < aliasedModuleNames.length; i++) {
            try {
                if (aliasedModuleNames[i] === '.') {
                    throw new Error('modules cannot be relative');
                }
                var aliasedModuleName = aliasedModuleNames[i].replace(/[\\/]*$/, ''); // Strip trailing slashes
                var codePath = resolve(aliasedModuleName);
                //var rootModule = aliasedModuleName.replace(/[/\\].*/, '');
                //var packageDir = path.dirname(resolve(rootModule + '/package.json'));
                //relativeModuleName = rootModule + '/' + path.relative(packageDir, aliasedModuleName);
                resolveError = null;
                break;
            } catch (e) {
                resolveError = e;
            }
        }
        if (resolveError) {
            return callback(resolveError);
        }

        var extension = codePath.split('.').pop().toLowerCase();
        fs.readFile(codePath, {encoding: 'utf-8'}, function (error, source) {
            if (error) return callback(error);
            if (typeof extensions[extension] !== 'function') {
                return callback(new Error('Unknown extension for ' + moduleName + ': ' + extension));
            }
            var callbackCalled = false;
            try {
                var jsAmd = extensions[extension](source, moduleName, relativeModuleName, function (error, jsAmd) {
                    if (callbackCalled) return;
                    callbackCalled = true;
                    if (error) return callback(error);
                    transformIfNeeded(jsAmd, options, callback);
                });
            } catch (e) {
                callbackCalled = true;
                return callback(e)
            }
            if (typeof jsAmd === 'string') {
                callbackCalled = true;
                transformIfNeeded(jsAmd, options, callback);
            }
        });
    }

    function lastPathPart(url) {
        url = url.replace(/\?.*/, '');
        return decodeURIComponent(url.replace(/\/$/, '').split('/').pop());
    };
    var loaderMatch = function (url) {
        console.log('loader', url, /^\/?$/.test(url));
        return /^\/?$/.test(url);
    };
    var moduleMatch = lastPathPart;
    if (options.loader) {
        loaderMatch = UriTemplateCodegen(options.loader).fromUri;
    }
    if (options.template) {
        var moduleMatches = [];
        if (typeof options.template === 'string') {
            moduleMatches.push({
                fromUrl: UriTemplateCodegen(options.template).fromUri,
                toModule: function (match) {
                    for (var key in match) {
                        return match[key];
                    }
                }
            });
        } else {
            [].concat(options.template).forEach(function (map) {
                for (var key in map) {
                    moduleMatches.push({
                        fromUrl: UriTemplateCodegen(map[key]).fromUri,
                        toModule: UriTemplateCodegen(key).fill
                    });
                }
            });
        }
        moduleMatch = function (url) {
            for (var i = 0; i < moduleMatches.length; i++) {
                var match = moduleMatches[i].fromUrl(url);
                console.log(match, url);
                if (match) return moduleMatches[i].toModule(match);
            }
        };
    }

    var api = {
        require: amdServer.require,
        dynamic: function (request, response, next) {
            if (!next) {
                next = function (error) {
                    response.setHeader('Content-Type', 'text/plain');
                    if (error) {
                        response.statusCode = 500;
                        response.end(error.stack || error.message || error + "");
                    } else {
                        response.statusCode = 404;
                        response.end('404 Not Found');
                    }
                };
            }
            if (loaderMatch(request.url)) {
                return cachedOrCode(function (callback) {
                    transformIfNeeded(amdCode(options), options, callback);
                });
            }

            var moduleName = moduleMatch(request.originalUrl || request.url);
            if (!moduleName) return next();

            return cachedOrCode(function (callback) {
                lookupCode(moduleName, function (error, amdJs) {
                    if (error) return callback(error);
                    jsCode = 'define._n=' + JSON.stringify(moduleName) + ';' + amdJs;
                    callback(null, jsCode);
                });
            });

            function cachedOrCode(codeFn) {
                codeFn(function (error, jsCode) {
                    if (typeof jsCode === 'string') {
                        jsCode = new Buffer(jsCode, 'utf-8');
                    }

                    if (error) {
                        response.setHeader('Content-Type', 'text/plain');
                        response.statusCode = 400;
                        response.end(error.stack || error.message);
                        return;
                    }

                    var etag = calculateEtag(jsCode);
                    if (etag && request.headers['if-none-match'] === etag) {
                        response.statusCode = 304;
                        response.end();
                        return;
                    }

                    response.setHeader('Content-Type', 'application/javascript');
                    response.setHeader('Content-Length', jsCode.length);
                    if (etag) response.setHeader('ETag', etag);
                    response.end(jsCode);
                });
            }
        }
    };
    return api;
};
