(function (global) {
	var require = global['require'] = function (nameOrDeps, func) {
		if (func) {
            if (typeof nameOrDeps === 'string') {
                nameOrDeps = [nameOrDeps];
            }
			for (var i = 0; i < nameOrDeps.length; i++) {
				isRequired[nameOrDeps[i]] = 1;
			}
			return define(0, nameOrDeps, func)
		} else {
			isRequired[nameOrDeps] = 1;
			// For efficiency, don't scan if we already have a result - costs us ~10 bytes, but probably worth it
			if (!modules[nameOrDeps]) {
				define(0, [nameOrDeps], 1)
			}
			if (nameOrDeps in modules) return modules[nameOrDeps];
			throw nameOrDeps;
		}
	};
	var counter = 0;
	var preventScan = 0;
	var define = global['define'] = function (name, deps, factory) {
		if (!name || (name + "" === name)) {
			// We were given a name (or a falsy value to indicate no module name at all)
			if (!factory) {
				factory = deps;
				deps = [];
			}
		} else {
			factory = deps || name;
			// Two arguments -> actual deps in the name, otherwise only one argument
			deps = deps ? name : [];
			name = define._n || ('.' + counter++);
		}
		pending.push([name, deps, factory]);
		if (name) modulesPending[name] = 1;

		scanForReadyModules();
		define._n = 0;
	};
	var isRequired = {};
	var pending = define._p = [];
	var modulesPending = {};
	var modules = {};

	function scanForReadyModules() {
		var item;
        var missingModules;
		if (preventScan) return;
		preventScan = 1;
		for (var pendingIndex = 0; item = pending[pendingIndex]; pendingIndex++) {
            if (!pendingIndex) {
                missingModules = [];
            }
			var name = item[0];
			if (!name || isRequired[name]) {
				var deps = item[1];
				var value = item[2];

				var args = [];
				var depName;
				for (var j = 0; j < deps.length; j++) {
					args[j] = modules[depName = deps[j]];
					// Use item as a flag for whether we're ready to go
					item = (depName in modules) && item;
					// Mark all our dependencies as required, restarting if necessary
					if (!isRequired[depName]) {
						pendingIndex = isRequired[depName] = -1;
					} else if (!item) {
                        missingModules.push(depName);
                    }
				}
				if (item) { // Ready to evaluate
					modulesPending[name] = 0;
					// Any anonymous declarations within this module are taken to redefine the whole module
					define._n = name;
					value = (typeof value === 'function') ? value.apply(global, args) : value;
					if (name && !modulesPending[name]) {
						modules[name] = value;
					}
					pending.splice(pendingIndex, 1);
					pendingIndex = -1;
				}
			}
		}
		define._n = preventScan = pendingIndex = 0;

		while (pendingIndex < missingModules.length) {
			missingModule(missingModules[pendingIndex++]);
		}
	};
	define.amd = {};

	/* CLIENT START */
	/* Amidala dynamic loading */
	var doc = document;
	var scripts = doc.getElementsByTagName('script');
	var script = doc.currentScript || scripts[scripts.length - 1];
	var currentScriptBase = (script && script.src || "").replace(/\/$/, '') + '/';

	var lookupModule = LOOKUP_FUNCTION;

	var scripts = {};
	function missingModule(name) {
		if (!scripts[name]) {
			script = scripts[name] = doc.createElement('script');
			script.onerror = function () {
				throw new Error('Missing module: ' + name);
			};
			script.src = lookupModule(name);
			doc.head.appendChild(script);
		}
	};
	/* CLIENT END */
})(this);
