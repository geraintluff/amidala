var assert = require('chai').assert;

var amidala = require('../../');

describe('AMD modules on Node.js', function () {
    it('constructs', function () {
        var amd = amidala({
            path: {"": __dirname + '/../amd'}
        });
    });

    it('loads "foo"', function (done) {
        var amd = amidala({
            path: {"": __dirname + '/../amd'}
        });

        amd.require('foo', function (foo) {
            assert.deepEqual(foo, "Foo");

            done();
        });
    });

    it('loads "bar" (depends on "foo")', function (done) {
        var amd = amidala({
            path: __dirname + '/../amd'
        });

        var bar = amd.require('bar', function (bar) {
            assert.deepEqual(bar, "FooBar");

            var foo = amd.require('foo');
            assert.deepEqual(foo, "Foo");

            done();
        });
    });
});
